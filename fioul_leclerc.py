import requests
import sys
from bs4 import BeautifulSoup

def help():
	print('Leclerc scraping Fioul price')
	print('usage: python3 ' + sys.argv[0] + ' [ALL|500|1000|2000] --verbose --standard --qualy-plus')
	print(' ')

def scrap():
	url = "http://www.sodif.fioul.leclerc/calcul-prix-fioul"
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')
	table = soup.find('table', {'class': 'tableau-tarifs'})
	return table

def date():
	table = scrap()
	date = table.find('caption')
	for d in date:
		d = d.replace('caption','')
		print(d)

def affichage(td):
	if standard == 'true':
		for t in td :
			t = t.replace('td','')
			if verbose == 'true':
				print('Fioul Standard : ' + t)
			else:
				t = t.replace(" €TTC","")
				print(t)

	if qualy_plus == 'true':
		td = td.findNext('td')
		for t in td :
			t = t.replace('td','')
			if verbose == 'true':
				print('Fioul Qualy-Plus : ' + t)
			else:
				t = t.replace(" €TTC","")
				print(t)

def L500():
	table = scrap()
	td = table.find('td', text='de 500 à 999 L')
	td = td.findNext('td')
	affichage(td)


def L1000():
	table = scrap()
	td = table.find('td', text='de 1000 à 1999 L')
	td = td.findNext('td')
	affichage(td)

def L2000():
	table = scrap()
	td = table.find('td', text='2000 L et +')
	td = td.findNext('td')
	affichage(td)


if __name__ == '__main__':

	if len(sys.argv) == 1:
		help()
		sys.argv.append('ALL')
		sys.argv.append('--verbose')
		sys.argv.append('--standard')
		sys.argv.append('--qualy-plus')

	else:
		if sys.argv[1] == '--help':
			help()
			exit()

		if len(sys.argv) > 5:
			help()
			exit()

	verbose = 'false'
	qualy_plus = 'false'
	standard = 'false'
	litre = 'NULL'

	list = sys.argv[1:]

	for value in list:
		if value == '--verbose':
			verbose = 'true'
		elif value == '--qualy-plus':
			qualy_plus = 'true'
		elif value == '--standard':
			standard = 'true'
		elif value == 'ALL':
			litre = 'ALL'
		elif value == '500':
			litre = '500'
		elif value == '1000':
			litre = '1000'
		elif value == '2000':
			litre = '2000'
		else:
			help()
	if standard == 'false' and qualy_plus == 'false':
		standard = 'true'
		qualy_plus = 'true'

	if verbose == 'true':
		date()

	if litre == 'ALL':
		if verbose == 'true':
			print(' ')
			print('500 à 999 L')
		L500()
		if verbose == 'true':
			print(' ')
			print('1000 à 1999 L')
		L1000()
		if verbose == 'true':
			print(' ')
			print('2000 L et + ')
		L2000()

	if litre == '500':
		if verbose == 'true':
			print(' ')
			print('500 à 999 L')
		L500()

	if litre == '1000':
		if verbose == 'true':
			print(' ')
			print('1000 à 1999 L')
		L1000()

	if litre == '2000':
		if verbose == 'true':
			print(' ')
			print('2000 L et + ')
		L2000()

	if verbose == 'true':
		print(' ')

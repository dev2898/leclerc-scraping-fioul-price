A simple script python for scrap the fioul price on http://www.sodif.fioul.leclerc/calcul-prix-fioul

Disclamer : I used to run this script every 24 hours, which resulted in my IP being banned from the website. After stopping the script for several days, the ban was lifted.

usage :
```
python3 fioul_leclerc.py [ALL|500|1000|2000] --verbose --standard --qualy-plus
```

ALL for show price of all liter

500 for show price of liter 500 to 999 L

1000 for show price of liter 1000 to 1999 L

2000 for show price of liter 2000 to +

--verbose for show detail. Without this option, show only the price, useful for graphing

--standard for only show standard fuel

--qualy-plus for only show quality+ fuel

exemple :

```
$ python3 fioul_leclerc.py ALL --verbose
Tarifs du mardi  4 octobre 2022

500 à 999 L
Fioul Standard : 1.629 €TTC
Fioul Qualy-Plus : 1.649 €TTC

1000 à 1999 L
Fioul Standard : 1.599 €TTC
Fioul Qualy-Plus : 1.619 €TTC

2000 L et +
Fioul Standard : 1.591 €TTC
Fioul Qualy-Plus : 1.611 €TTC
```

```
$ python3 fioul_leclerc.py 500 --standard
1.629
```
